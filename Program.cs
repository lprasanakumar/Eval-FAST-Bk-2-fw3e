﻿using FortCodeExercises.Exercise1.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("enter machine type");
            int type = int.Parse(Console.ReadLine());

            MachineFactory machineFactory = new ConcreteMachineFactory();
            IMachine machine = machineFactory.GetMachine(type);
            var description = "";
            description += " ";
            description += machine.Color + " ";
            description += machine.Name;
            description += " ";
            description += "[";
            description += getMaxSpeed(type, machine.Description) + "].";

            Console.ReadLine();
        }
        public static int getMaxSpeed(int machineType, bool noMax = false)
        {
            var max = 70;
            if (machineType == 1 && noMax == false) max = 70;
            else if (noMax == false && machineType == 2) max = 60;
            else if (machineType == 0 && noMax == true) max = 80;
            else if (machineType == 2 && noMax == true) max = 90;
            else if (machineType == 4 && noMax == true) max = 90;
            else if (machineType == 1 && noMax == true) max = 75;
            return max;
        }
    }
    public abstract class MachineFactory
    {
        public abstract IMachine GetMachine(int type);
    }
    class ConcreteMachineFactory : MachineFactory
    {
        public override IMachine GetMachine(int type)
        {
            switch (type)
            {
                case 0:
                    return new MachineZeroType();
                case 1:
                    return new MachineOneType();
                case 2:
                    return new MachineTwoTypes();
                case 3:
                    return new MachineThreeType();
                case 4:
                    return new MachineFourType();
                default:
                    return new MachineDefaultType();
            }
        }
    }
    public interface IMachine
    {
        public string Name { get; set; }
        public bool Description { get; set; }
        public string Color { get; set; }
        public string TrimColor { get; set; }
        public bool IsDark { get; set; }
    }
    class MachineDefaultType : IMachine
    {
        public string Name
        {
            get { return Name; }
            set { Name = ""; }
        }
        public bool Description
        {
            get { return Description; }
            set { Description = true; }
        }
        public string Color
        {
            get { return Color; }
            set { Color = "white"; }
        }
        public string TrimColor
        {
            get { return TrimColor; }
            set { TrimColor = "white"; }
        }
        public bool IsDark
        {
            get { return IsDark; }
            set { IsDark = false; }
        }
    }
    class MachineZeroType : IMachine
    {
        public string Name
        {
            get { return Name; }
            set { Name = "bulldozer"; }
        }
        public bool Description
        {
            get { return Description; }
            set { Description = true; }
        }
        public string Color
        {
            get { return Color; }
            set { Color = "red"; }
        }
        public string TrimColor
        {
            get { return TrimColor; }
            set { TrimColor = "red"; }
        }
        public bool IsDark
        {
            get { return IsDark; }
            set { IsDark = true; }
        }
    }
    class MachineFourType : IMachine
    {
        public string Name
        {
            get { return Name; }
            set { Name = "car"; }
        }
        public bool Description
        {
            get { return Description; }
            set { Description = false; }
        }
        public string Color
        {
            get { return Color; }
            set { Color = "brown"; }
        }
        public string TrimColor
        {
            get { return TrimColor; }
            set { TrimColor = "brown"; }
        }
        public bool IsDark
        {
            get { return IsDark; }
            set { IsDark = false; }
        }
    }
    class MachineOneType : IMachine
    {
        public string Name
        {
            get { return Name; }
            set { Name = "crane"; }
        }
        public bool Description
        {
            get { return Description; }
            set { Description = true; }
        }
        public string Color
        {
            get { return Color; }
            set { Color = "blue"; }
        }
        public string TrimColor
        {
            get { return TrimColor; }
            set { TrimColor = "blue"; }
        }
        public bool IsDark
        {
            get { return IsDark; }
            set { IsDark = false; }
        }

    }
    class MachineThreeType : IMachine
    {
        public string Name
        {
            get { return Name; }
            set { Name = "truck"; }
        }
        public bool Description
        {
            get { return Description; }
            set { Description = false; }
        }
        public string Color
        {
            get { return Color; }
            set { Color = "yellow"; }
        }
        public string TrimColor
        {
            get { return TrimColor; }
            set { TrimColor = "yellow"; }
        }
        public bool IsDark
        {
            get { return IsDark; }
            set { IsDark = false; }
        }
    }
    class MachineTwoTypes : IMachine
    {
        public string Name
        {
            get { return Name; }
            set { Name = "tractor"; }
        }
        public bool Description
        {
            get { return Description; }
            set { Description = true; }
        }
        public string Color
        {
            get { return Color; }
            set { Color = "green"; }
        }
        public string TrimColor
        {
            get { return TrimColor; }
            set { TrimColor = "green"; }
        }
        public bool IsDark
        {
            get { return IsDark; }
            set { IsDark = true; }
        }

    }
}